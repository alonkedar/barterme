var mongoose = require("mongoose");

var schema = mongoose.Schema({
    title: {type: String, required: true},
    estimated_value: {type: Number, required: true},
    // image_url: {type: String, required: true},
    owner_id: {type: mongoose.Schema.ObjectId, required: true},
    description: String,
    location: {type: [Number], index: '2d'}
});


schema.statics.listNearItems = function(lat, lng, maxDistance, userId, cb) {
    // convert kilometers to radians
    var distance = maxDistance / 1000 / 6371;

    // build location object
    var location = [lng, lat];

    // run querys
    return Model.find({location: {$nearSphere: location, $maxDistance: distance}, owner_id: {$ne:userId}}, cb);
}

schema.statics.listUserItems = function (userId, cb) {
    return Model.find({owner_id: userId}, cb);
}

schema.statics.add = function (user, newItem, cb) {
    newItem.owner_id = user.id;
    newItem.save(function (err, item) {
        // if (!err) {
        //     user.items_ids.push(item.owner_id);
        // }
        cb(err);
    });
}

schema.statics.remove = function (user, item, cb) {
    item.remove(function (err, item) {
        cb(err);
    });
}

var Model = mongoose.model("Item", schema);
module.exports = Model;