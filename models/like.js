var mongoose = require("mongoose");

var schema = mongoose.Schema({
    item_id: {type: mongoose.Schema.Types.ObjectId, ref: "Item", required: true},
    user_id: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true}
});

var Model = mongoose.model("Like", schema);
module.exports = Model;
