var mongoose = require("mongoose");

var schema = mongoose.Schema({
    item_id1: {type: mongoose.Schema.Types.ObjectId, ref: "Item", required: true},
    item_id2: {type: mongoose.Schema.Types.ObjectId, ref: "Item", required: true},
    user_id1: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true},
    user_id2: {type: mongoose.Schema.Types.ObjectId, ref: "User", required: true}
});

var Model = mongoose.model("Match", schema);
module.exports = Model;
