var mongoose = require("mongoose");
var crypto = require("crypto");
var sha1 = require("sha1");

var schema = mongoose.Schema({
	username: {type: String, unique: true},
	token: { type: String, index: { unique: true }},
	password: String,
	image_url: String,
	items_ids: [mongoose.Schema.ObjectId],
	phone: { type: String, index: { unique: true }}
});

schema.statics.findByUsernameAndPassword = function(username, password, cb) {
    return Model.findOne({username: username, password: sha1(password)}, cb);
}

schema.statics.findByToken = function(token, cb) {
    return Model.findOne({token: token}, cb);
}

// generate a token before saving the object (it wont happen on update)
schema.pre('save', function(next) {
    if (this.token) {
        next();
        return;
    }
    // save 'this' for use in crypto callback
    var self = this;
    crypto.randomBytes(48, function(ex, buf) {
        self.token = buf.toString('hex');
        next();
    });
});

var Model = mongoose.model("User", schema);
module.exports = Model;