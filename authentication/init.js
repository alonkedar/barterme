function init(app) {
    var User = require("../models/user");
    var passport = require('passport')
        , LocalStrategy = require('passport-local').Strategy
        , usernameAndPasswordStrategy = new LocalStrategy(
        {usernameField: 'username', passwordField: 'password'}, User.findByUsernameAndPassword)

        , serializeUser = require('../commands/serializeUser')()
        , deserializeUser = require('../commands/deserializeUser')();

    passport.use(usernameAndPasswordStrategy);
    passport.serializeUser(serializeUser);
    passport.deserializeUser(deserializeUser);

    app.use(passport.initialize())
    app.use(passport.session())
}

module.exports = init;