var express = require('express');
var router = express.Router();
var middlewares = require("../authentication/middleware");
var Item = require('../models/item.js');

// all endpoints with "~" are currently logged in user relevant

// get my items
router.get('/list_items', middlewares.ensureAuthenticated(), function (req, res) {
    Item.listUserItems(req.user.id, function (err, items) {
        if(err) res.status(404).json({error: err});
        else res.json(items);
    });
});

// add item to my items
router.post('/add', middlewares.ensureAuthenticated(), function (req, res, next) {
    var lng = req.body.lng, lat = req.body.lat;
    if (!lng) {
        res.status(500).json({error: 'missing location'});
        return;
    }

    if (!lat) {
        res.status(500).json({error: 'missing location'});
        return;
    }

    var location = [lng, lat];
    var newItem = new Item(req.body);
    newItem.location = location;
    if (!newItem) {
        res.status(500).json({error: 'not enough data to build item object'});
        return;
    }
    Item.add(req.user, newItem, function (err) {
        if(err) res.status(404).json({error: err});
        else res.send('item was added successfully');
    });
});

router.delete('/remove/:item_id', middlewares.ensureAuthenticated(), function (req, res, next) {
    var itemId = req.params.item_id;

    if (!itemId) {
        res.status(500).json({error: 'missing item id'});
        return;
    }
    var item = Item.find({_id:itemId});
    if (!item) {
        res.status(500).json({error: 'no item match to the specified id'});
        return;
    }
    Item.remove(req.user, item, function (err) {
        if(err) res.status(404).json({error: err});
        else {
            // todo: remove item from req.user.items_ids
            res.send('item was removed successfully');
        }
    });
});

// get relevant items
router.get('/', middlewares.ensureAuthenticated(), function (req, res, next) {
    if (!req.query.latitude) {
        res.status(500).json({error: 'missing location'});
        return;
    }

    if (!req.query.longitude) {
        res.status(500).json({error: 'missing location'});
        return;
    }

    if (!req.query.radius) {
        res.status(500).json({error: 'missing max distance'});
        return;
    }

    Item.listNearItems(req.query.latitude, req.query.longitude, req.query.radius, req.user.id, function (err, items) {
        if (err) return next(err);
        res.json(items);
        return;
    });
});

// update item with item_id
router.put('/~/:item_id', middlewares.ensureAuthenticated(), function (req, res, next) {
    var updates = req.body;
    if(!updates)
        res.status(500).send('no update parameters included');

    Item.update({_id: req.params.item_id}, {$set:updates})
});

module.exports = router;