var express = require('express');
var User = require("../models/user");
var sha1 = require("sha1");
var router = express.Router();
var middlewares = require("../authentication/middleware");
var passport = require('passport');


router.post('/signup', function (req, res, next) {
    var userObject = req.body;
    if (!userObject.phone) {
        res.status(400).json({error: 'phone missing'});
        return;
    }
    if (!userObject.password) {
        res.status(400).json({error: 'password missing'});
        return;
    }
    // hash password
    userObject.password = sha1(userObject.password);
    var user = new User(userObject);
    if (!user) {
        res.status(400).json({error: 'not enough data to build user object'});
        return;
    }

    user.save(function (err, createdUser) {
        if (err) {
            res.status(404).json({error: err});
        } else {
            // res.json(createdUser);
            req.login(user, function (err) {
                if (err) {
                    console.log(err);
                }
                res.json({status: "success"});
            });
        }
    });
});

router.post('/login', passport.authenticate('local', {
    failureRedirect: 'fail'
}), function(req, res) {
    res.json({status: "success"});
});

router.get('/logout', middlewares.ensureAuthenticated(), function(req, res){
    req.logout();
    res.json({status: "success"});
});

router.put('/update_details', middlewares.ensureAuthenticated(), function (req, res) {
    var username = req.body.username;
    var phone = req.body.phone;
    var details = {};

    if(!(username || phone)) {
        res.status('500').send('missing details to update');
        return;
    }

    if(username) { details.username = username; }
    if(phone) { details.phone = phone; }

    req.user.update(details, function (err, details) {
        if (err) return err;
        res.json({status: "success"});
    });
});

// passport test
router.get('/sample', middlewares.ensureAuthenticated(), function (req, res) {
    res.send('you are logged in');
});

router.get('/fail', function (req, res) {
    res.send('authentication failed');
});

router.put('/change_password', middlewares.ensureAuthenticated(), function (req, res, next) {
    if (!req.body.currentPassword) {
        res.status(500).json({error: 'current password missing'});
        return;
    } else if (sha1(req.body.currentPassword) !== req.user.password) {
        res.status(500).json({error: 'current password is incorrect'});
        return;
    }
    if (!req.body.newPassword) {
        res.status(500).json({error: 'new password missing'});
        return;
    }

    var user = req.user;
    user.password = sha1(req.body.newPassword);
    user.save(function (err, user) {
        if (err) {
            res.status(404).json({error: err});
        } else {
            res.send("password was updated successfully");
        }
    })
});

module.exports = router;
