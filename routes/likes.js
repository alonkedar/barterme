var express = require('express');
var router = express.Router();
var Like = require('../models/like.js');
var middlewares = require("../authentication/middleware");

var Item = require('../models/item.js');
var User = require('../models/user.js');


// get my matches
router.get('/~/matches', function (req, res, next) {

});

// mark item as liked
router.post('/like/:item_id', middlewares.ensureAuthenticated(), function (req, res, next) {
    var itemId = req.params.item_id;
    var likeData = {
        user_id: req.user.id,
        item_id: itemId
    }
    var like = new Like(likeData);
    if (!like) {
        res.status(500).send('not enough data to build user interest object');
        return;
    }

    like.save(function (err, like) {
        if (err) {
            res.status(404).json({error: err});
            return;
        }
    });

    getItem(itemId)
        .then((item) => Promise.resolve({ ownerId: item.owner_id, itemsIds: req.user.items_ids }))
        .then(getItemMatches)
        .then(getUserPhone)
        .then((phone) => { res.send("u got a match!!! here the owner's phone: " + phone)})
        .catch( (e)=> res.status(500).send(e) );
});

function getItem(itemId){
    return new Promise((resolve, reject) => {
        Item.findOne({_id: itemId}).exec()
            .then((item) => {
                if(item) resolve(item);
                else reject('no item for specified id');
            });
    })
}

function getItemMatches(params){
    return new Promise((resolve, reject) => {
        Like.find({item_id: {$in: params.itemsIds}}).exec()
            .then((matches) => {
                if (matches) resolve(params.ownerId);
                else reject('no matches found');
            });
    })
}


function getUserPhone(userId){
    return new Promise((resolve, reject) => {
        User.findOne({_id: userId}).exec()
            .then((user) => {
                if(user) resolve(user.phone);
                else reject('no user found');
            })
    })
}

module.exports = router;

