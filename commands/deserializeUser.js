'use strict';

var User = require('../models/user')
module.exports = function () {
    return function deserializeUser(obj, cb) {
        User.findByToken(obj.token, cb);
    };
};