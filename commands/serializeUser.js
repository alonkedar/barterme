'use strict';
module.exports = function() {
    return function serializeUser(user, cb) {
        cb(null, {token: user.token});
    };
};